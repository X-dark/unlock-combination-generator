#! env python3

from random import randrange
from sys import exit

def existing_combination(test_number, other_side, combinations):
  for number in other_side:
    if (test_number + number) in combinations:
      return True
  return False

def update_combinations(test_number, other_side, combinations):
  for number in other_side:
    combinations.append(test_number + number)
  return combinations

def find_number(already_found, combine_with):

  global biggest_number, combinations, max_retry, debug, increment_size

  new_number = randrange(biggest_number) + 1
  retry = 0

  while new_number in already_found or new_number in combine_with or existing_combination(new_number, combine_with, combinations):
    if retry > max_retry:
      biggest_number = biggest_number + increment_size
      retry = 0
      if debug:
        print(f'Increasing max number to {biggest_number}')
    retry = retry + 1
    new_number = randrange(biggest_number) + 1

  combinations = update_combinations(new_number, combine_with, combinations)
  return new_number


debug = False
red_size = 14
blue_size = 14
biggest_number = 19
increment_size = 10
max_retry = 1000000
reds = []
blues = []
combinations = []

for i in range(max(red_size, blue_size)):

  if i < red_size:
    new_number = find_number(reds, blues)
    reds.append(new_number)

    if debug:
      print(f'red #{i+1} found: {new_number}')

  if i < blue_size:
    new_number = find_number(blues, reds)
    blues.append(new_number)

    if debug:
      print(f'blue #{i+1} found: {new_number}')

reds.sort()
blues.sort()
combinations.sort()

print(f'reds: {reds}')
print(f'blues: {blues}')

if debug:
  print(f'combinations: {combinations}')

# vim: set sw=2 ts=2 et:
